<?php
/**
 * @file
 * Morris omniture plugin interface.
 */

/**
 * All morris omniture var plugins are required to implement
 * this interface. In most cases it will be enough to use
 * the template that has been set-up: var_base which implements
 * all these in a basic form. Normally the logic there is enough
 * except for those plugins requiring settings.
 */
interface morris_omniture_ivar {
  /**
   * Get full unique name of plugin.
   *
   * @return string
   *   The unique name of plugin.
   */
  public function get_name();

  /**
   * Get group plugin belongs to.
   *
   * @return string
   *   The group which the plugin belongs to. This is used to organize
   *   plugins within a linked list on the UI.
   */
  public function get_group();

  /**
   * Get title shown on admin screens.
   *
   * @return string
   *   The title shown on admin screens/UI identifying the plugin.
   */
  public function get_title();

  /**
   * Get description of plugin that will be shown on admin screens. 
   * 
   * This description should make sense to none technical
   * users who would be responsible for mapping variables.
   * 
   * @return string
   *   The UI description for the plugin.
   */
  public function get_description();

  /**
   * Title shown to identify plugin as unique item on admins screens.
   * 
   * This should normally include the group
   * when it has been defined. Otherwise, it would be difficult
   * to differentiate plugins with the same name but different
   * context. A good example might be node->created and
   * user->created. Where node and user are groups and created
   * is the name.
   *
   * @return string
   *   The UI title for the plugin.
   */
  public function ui_title();

  /**
   * Determine whether value can be overriden by mapping on a page.
   *
   * @return bool
   *   Whether mapping value is overrideable per request.
   */
  public function overridable();

  /**
   * Get admin settings form.
   *
   * Plugins that have settings should use this method
   * to return the settings. The settings should
   * be specified in the standard way they would be for a
   * form. Plugins that do not use settings should return
   * null or an empty array.
   * 
   * @param array &$form_state
   *   The variable mapping form state.
   *
   * @return array $form_state
   *   When mapping contains settings for the plugin mapping form.
   */
  public function settings(&$form_state);

  /**
   * Submit handler for settings form.
   *
   * @param array $values
   *   Plugin mapping form settings submitted values.
   */
  public function settings_submit($values);

  /**
   * The settings override form.
   *
   * Only plugins that allow settings to be overridden
   * when used within a context should use this method.
   * Othwerwise, it should return null or an empty array.
   * 
   * @param StdClass $context
   *   A context object.
   *
   * @return array
   *   The overrideable settings for a plugin.
   */
  public function override_form($context);

  /**
   * Submit handler for override form.
   *
   * @param array $values
   *   Submitted values for override settings form.
   */
  public function override_submit($values);

  /**
   * Action applied when variable is unmapped.
   *
   * This method will be called when a variable mapping is deleted. This
   * can be used to do any necessary clean-up such as; deleting variables
   * or updating table(s) in the database.
   */
  public function unmap();

  /**
   * The value for plugin.
   *
   * The value placed on the page for the plugin. In most cases
   * this should be a string. However, in some cases pure JavaScript
   * must be used on the page. To prevent XSS attacks the function
   * morris_omniture_js_snippet_wrapper should be used to wrap javaScript
   * code.
   *
   * @return mix
   *   The value sent to Site Catayst for the mapping.
   */
  public function val();
}
