<?php
/**
 * @file
 * The mapping repository.
 */

class MorrisOmniture_Mapping_Repository implements DrupalEntityControllerInterface {

  /**
   * Class constructor.
   * 
   * @param str $entity_type
   *   The entity type.
   */
  public function __construct($entity_type) {

  }

  /**
   * Reset internal entity cache.
   * 
   * @param array $ids
   *   Collection of entity IDs.
   */
  public function resetCache(array $ids = NULL) {

  }

  /**
   * Load entities from db.
   * 
   * @param array $ids
   *   Collection of entity IDs.
   * 
   * @param array $conditions
   *   Conditions to apply when loading entities.
   * 
   * @return array
   *   Collection entity instances.
   */
  public function load($ids = array(), $conditions = array()) {

    $query = $this->_makeBaseSelectQuery($conditions);

    if (!empty($ids)) {
      $query->condition('m.id', $ids);
    }

    module_invoke_all('morris_omniture_mapping_load_query_alter', $query, $ids, $conditions);

    $result = $query->execute();

    $mappings = $this->_mapSelectResult($result);

    return $mappings;

  }

  /**
   * Save mapping to persistent storage mechanism.
   *
   * @param MorrisOmniture_Mapping $mapping
   *   The mapping to persist.
   */
  public function persist(MorrisOmniture_Mapping $mapping) {

    $transaction = db_transaction();

    try {

      $data = array(
        'context_id' => $mapping->getContext()->name,
      );

      if ($mapping->getId() !== NULL) {

        db_update('morris_omniture_mapping')->fields($data)->condition('id', $mapping->getId())->execute();

      }
      else {

        $id = db_insert('morris_omniture_mapping')->fields($data)->execute();

        $mapping->setId($id);

      }

      // Allow other modules to react.
      module_invoke_all('morris_omniture_mapping_persist', $mapping);

      // Invalidate context cache.
      context_invalidate_cache();

    }
    catch (Exception $e) {
      $transaction->rollback();
      throw $e;
    }

  }

  /**
   * Physically remove mappings and dependencies from database.
   * 
   * @param array $ids
   *   Collection of mapping primary keys.
   */
  public function purgeById($ids) {

    if (empty($ids)) {
      return;
    }

    // Gather all the names of context that need to be deleted.
    $context = array();
    $query = db_select('morris_omniture_mapping', 'm');
    $query->condition('m.id', $ids);
    $query->fields('m', array('context_id'));
    $result = $query->execute();

    while ($row = $result->fetchAssoc()) {
      $context[] = $row['context_id'];
    }

    $transaction = db_transaction();

    try {

      // Remove anything with foreign key to morris omniture mappging.
      module_invoke_all('morris_omniture_mapping_purge', $context);

      // Delete the mapping itself.
      $query = db_delete('morris_omniture_mapping');
      $query->condition('id', $ids);
      $r = $query->execute();

      /*
       * Delete context
       *
       * Implementation pulled directly from context_delete though this
       * uses the query builder rather than hard-coded query.
       */
      $query = db_delete('context');
      $query->condition('name', $context);
      $query->execute();
      context_invalidate_cache();

      return $r;

    }
    catch (Exception $e) {
      $transaction->rollback();
      throw $e;
    }

  }

  /**
   * Create base select query.
   * 
   * @param array $conditions
   *   Conditions to apply when building base query.
   * 
   * @return object
   *   The query object.
   */
  protected function _makeBaseSelectQuery($conditions) {

    $query = db_select('morris_omniture_mapping', 'm');
    $query->fields('m');

    if (isset($conditions['mapping'])) {
      foreach ($conditions['mapping'] as $field => $value) {
        if (strpos($field, '.') === FALSE) {
          $query->condition("m.$field", $value);
        }
        else {
          $query->condition($field, $value);
        }
      }
    }

    return $query;

  }

  /**
   * Map raw result to entity object(s).
   *
   * @param object $result
   *   Query result pointer.
   * 
   * @return array
   *   Collection of MorrisOmniture_Mapping instances.
   */
  protected function _mapSelectResult($result) {

    /*
     * Map raw data to create collection of mappings.
     */
    $mappings = array();

    /*
     * Map raw data to domain object.
     */
    while ($row = $result->fetchAssoc()) {

      if (!isset($mappings[$row['id']])) {

        // Create new domain object.
        $mapping = new MorrisOmniture_Mapping();

        // Map row to domain object.
        $mapping->setId($row['id']);

        /*
         * Associated the context with mapping.
         */
        $mapping->setContext((object) array(
          'name' => $row['context_id'],
        ));

      }
      else {
        $mapping = $mappings[$row['id']];
      }

      module_invoke_all('morris_omniture_mapping_map_select_result', $mapping, $row);

      if (!isset($mappings[$row['id']])) {
        $mappings[$row['id']] = $mapping;
      }

    }

    return $mappings;

  }

}
