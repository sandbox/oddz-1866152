<?php
/**
 * @file
 * Variable mapping configuration form namespace.
 */

/**
 * Interactive variable mapping data grid.
 * All functions contained within this file are associated with the
 * variable mapping data grid. No functions within this file should be
 * relied on from outside the context of the interactive variable
 * mapping data grid page.
 */

/**
 * Form constructor for variable mapping form.
 * 
 * @see morris_omniture_mappings_form_validate_prop
 * @see morris_omniture_mappings_form_validate_plugin_group
 * @see morris_omniture_mappings_form_validate_plugin
 * @see morris_omniture_mappings_form_validate_grp
 * @see morris_omniture_mappings_form_remove_handler
 * @see morris_omniture_mappings_form_add_handler
 * @see morris_omniture_mappings_form_plugin_settings_handler
 * @see morris_omniture_mappings_form_remove_handler_cancel
 * 
 * @ingroup froms
 */
function morris_omniture_mappings_form($form, &$form_state) {

  $plugins = morris_omniture_get_vars();
  $form = array();

  /*
   * Control variable to determine whether user is allowed to add and
   * remove mappings. This is a way of preventing people from creating
   * mappings if they don't really know what they are doing. Instead
   * they will only be able to change settings for mappings but not
   * affect the structure of the table itself.
   */
  $allow_edit = morris_omniture_user_var_mapping_admin();
  $allow_remove = morris_omniture_user_var_mapping_admin();

  /*
   * ----------------------------------------------------------------------
   * The variable table.
   * ----------------------------------------------------------------------
   */
  $form['vars_wrapper'] = array(
    '#type' => 'container',
    '#attributes' => array('id' => 'vars-wrapper'),
  );

  $table_cls = array('draggable', 'tabledrag-leaf', 'add-new');

  $form['vars_wrapper']['vars'] = array(
    '#tree' => TRUE,
    '#attributes' => array('class' => $table_cls),
    '#type' => 'morris_omniture_mappings_form_var_table',
    '#header' => array(
      'id' => 'Variable',
      'plugin' => 'Plugin',
      'settings' => 'Settings',
    ),
    '#rows' => array(),
    '#empty' => 'No variables Have been Defined',
    '#caption' => 'SiteCatalyst Variables',
  );

  unset($table_cls);

  // When user is able to remove mappings add remove header.
  if ($allow_remove) {
    $form['vars_wrapper']['vars']['#header']['remove'] = 'Remove';
  }

  /*
   * ----------------------------------------------------------------------
   * Add new variable declaration.
   * ----------------------------------------------------------------------
   */
  if ($allow_edit) {

    $form['vars_wrapper']['vars']['new'] = array(
      '#weight' => -101,
      'id' => array(
        '#type' => 'textfield',
        '#title' => 'Variable',
        '#element_validate' => array('morris_omniture_mappings_form_validate_prop'),
      ),
      'plugin_wrapper' => array(
        '#type' => 'container',
        '#attributes' => array('id' => 'plugin-linkedlist'),
        'plugin_group' => array(
          '#type' => 'select',
          '#title' => 'Plugin',
          '#options' => array(
            '' => '--',
          ),
          '#attributes' => array('style' => 'width: 100%;'),
          '#parents' => array('vars', 'new', 'plugin_group'),
          '#ajax' => array(
            'callback' => 'morris_omniture_mappings_form_ajax_plugin',
            'wrapper' => 'plugin-linkedlist',
            'method' => 'replace',
          ),
          '#element_validate' => array('morris_omniture_mappings_form_validate_plugin_group'),
        ),
      ),
      'group' => array(
        '#type' => 'textfield',
        '#title' => 'Group',
        '#element_validate' => array('morris_omniture_mappings_form_validate_grp'),
      ),
      'add' => array(
        '#type' => 'submit',
        '#value' => 'Add',
        '#submit' => array('morris_omniture_mappings_form_add_handler'),
        '#executes_submit_callback' => TRUE,
        '#ajax' => array(
          'callback' => 'morris_omniture_mappings_form_ajax_var_add',
          'wrapper' => 'vars-wrapper',
          'method' => 'replace',
        ),
      ),
    );

  }

  /*
   * ----------------------------------------------------------------------
   * The data grid portion.
   * ----------------------------------------------------------------------
   */
  $vars = morris_omniture_get_props();
  $index = 0;

  foreach ($vars as $item) {

    $plugin = $item->getPlugin();
    $title = $plugin->ui_title();
    $settings = $plugin->settings($form_state);
    $has_settings = !empty($settings);

    $group = $item->getGrp() ? $item->getGrp() : 'undef';

    if (!isset($form['vars_wrapper']['vars'][$group])) {
      $form['vars_wrapper']['vars'][$group] = array(
        '#data' => htmlentities($group),
      );

      // The ungrouped items rise to the top and change label.
      if (!$item->getGrp()) {
        $form['vars_wrapper']['vars'][$group]['#data'] = 'Ungrouped';
        $form['vars_wrapper']['vars'][$group]['#weight'] = -100;
      }

    }

    $form['vars_wrapper']['vars'][$group]["{$index}"] = array(
      'id_wrapper' => array(
        '#type' => 'container',
        'label' => array(
          '#markup' => $item->getId(),
        ),
        'id' => array(
          '#type' => 'hidden',
          '#value' => $item->getId(),
          '#parents' => array('vars', "{$index}", 'id'),
        ),
      ),
      'plugin_wrapper' => array(
        '#type' => 'container',
        'label' => array(
          '#markup' => $title,
        ),
        'plugin_group' => array(
          '#type' => 'hidden',
          '#value' => $plugin->get_name(),
          '#parents' => array('vars', "{$index}", 'plugin_group'),
        ),
      ),
      'settings' => array(
        '#type' => 'button',
        '#value' => "Settings {$index}",
        '#disabled' => !$has_settings,
        '#post_render' => array('morris_omniture_mappings_form_proxy_btn'),
        '#ajax' => array(
          'callback' => 'morris_omniture_mappings_form_ajax_var_settings',
          'wrapper' => 'vars-wrapper',
          'method' => 'replace',
        ),
      ),
    );

    // When user is allowed to remove mappings add remove operation.
    if ($allow_remove) {
      $form['vars_wrapper']['vars'][$group]["{$index}"]['remove'] = array(
        '#type' => 'submit',
        '#value' => t("Remove {$index}"),
        '#submit' => array('morris_omniture_mappings_form_remove_handler'),
        '#disabled' => !$allow_remove,
        '#post_render' => array('morris_omniture_mappings_form_proxy_btn'),
        '#ajax' => array(
          'callback' => 'morris_omniture_mappings_form_ajax_var_remove',
          'wrapper' => 'vars-wrapper',
          'method' => 'replace',
        ),
      );
    }

    // The plugin settings.
    if (isset($form_state['values'], $form_state['values']['op']) && strpos($form_state['values']['op'], 'Settings') === 0) {
      $setting_index = (int) str_replace('Settings ', '', $form_state['values']['op']);
      if ($setting_index == $index) {

        $form['vars_wrapper']['vars'][$group]["{$index}_settings"] = $plugin->settings($form_state);

        $form['vars_wrapper']['vars'][$group]["{$index}_settings"]['save'] = array(
          '#type' => 'submit',
          '#value' => "Save $index",
          '#submit' => array('morris_omniture_mappings_form_plugin_settings_handler'),
          '#post_render' => array('morris_omniture_mappings_form_proxy_btn'),
          '#ajax' => array(
            'callback' => 'morris_omniture_mappings_form_ajax_plugin_settings',
            'wrapper' => 'vars-wrapper',
            'method' => 'replace',
          ),
        );

        $form['vars_wrapper']['vars'][$group]["{$index}_settings"]['cancel'] = array(
          '#type' => 'submit',
          '#value' => 'Cancel',
          '#submit' => array('morris_omniture_mappings_form_plugin_settings_handler_cancel'),
          '#post_render' => array('morris_omniture_mappings_form_proxy_btn'),
          '#ajax' => array(
            'callback' => 'morris_omniture_mappings_form_ajax_plugin_settings',
            'wrapper' => 'vars-wrapper',
            'method' => 'replace',
          ),
        );

      }
    }

    $index++;

  }

  // Get all plugins grouped.
  $grouped_plugins = morris_omniture_plugins_grouped();

  foreach (array_keys($grouped_plugins) as $group) {
    $form['vars_wrapper']['vars']['new']['plugin_wrapper']['plugin_group']['#options'][$group] = $group;
  }

  // The Plugin group.
  if (isset($form_state['values'], $form_state['values']['vars'], $form_state['values']['vars']['new'], $form_state['values']['vars']['new']['plugin_group'])) {
    $group = $form_state['values']['vars']['new']['plugin_group'];

    if (!empty($group)) {

      $plugins = morris_omniture_plugins_grouped();
      $options = array('' => '--');

      foreach ($plugins[$group] as $plugin) {
        $options[$plugin->get_name()] = $plugin->get_title();
      }

      $form['vars_wrapper']['vars']['new']['plugin_wrapper']['plugin'] = array(
        '#type' => 'select',
        '#options' => $options,
        '#parents' => array('vars', 'new', 'plugin'),
        '#required' => TRUE,
        '#attributes' => array('style' => 'margin-top: 5px; width: 100%;'),
        '#element_validate' => array('morris_omniture_mappings_form_validate_plugin'),
      );

    }

  }

  return $form;

}

/**
 * ----------------------------------------------------------------------
 * The Validation callbacks.
 * ----------------------------------------------------------------------
 */

/**
 * Render API callback: Validates mapping name.
 *
 * Ensures mapping name can be translated to JavaScript variable.
 *
 * This function is assigned as an #element_validate callback in
 * morris_omniture_mappings_form().
 */
function morris_omniture_mappings_form_validate_prop($element, &$form_state, $form) {

  if (isset($form_state['values']['op']) && strcasecmp($form_state['values']['op'], 'Add') === 0) {

    $id = $form_state['values']['vars']['new']['id'];

    if (empty($id)) {
      form_error($element, t('Prop required to create variable'));
    }
    elseif (!preg_match('/^[a-zA-Z_][0-9A-Za-z_]*?$/', $id)) {
      form_error($element, t('Prop may contain only alpha-numeric characters and underscores must begin with a character other than a number.'));
    }

  }

}

/**
 * Render API callback: Validates plugin group.
 *
 * Ensures plugin group has been selected item.
 *
 * This function is assigned as an #element_validate callback in
 * morris_omniture_mappings_form().
 */
function morris_omniture_mappings_form_validate_plugin_group($element, &$form_state, $form) {

  if (isset($form_state['values']['op']) && strcasecmp($form_state['values']['op'], 'Add') === 0) {

    if (!isset($form_state['values']['vars']['new']['plugin_group']) || empty($form_state['values']['vars']['new']['plugin_group'])) {
      form_error($element, t('Please select a plugin group than a sepcific plugin to map the variable.'));
    }

  }

}

/**
 * Render API callback: Validates plugin.
 *
 * Ensures plugin has been selected.
 *
 * This function is assigned as an #element_validate callback in
 * morris_omniture_mappings_form().
 */
function morris_omniture_mappings_form_validate_plugin($element, &$form_state, $form) {

  if (isset($form_state['values']['op']) && strcasecmp($form_state['values']['op'], 'Add') === 0) {

    if (!isset($form_state['values']['vars']['new']['plugin']) || empty($form_state['values']['vars']['new']['plugin'])) {
      form_error($element, t('Plugin must be choosen to create a variable mapping.'));
    }
    else {

      $mapped = entity_get_controller('morris_omniture_var')->load(NULL, array(
        'var' => array(
          'plugin' => $form_state['values']['vars']['new']['plugin'],
        ),
      ));

      if (!empty($mapped)) {
        form_error($element, t('Plugin has already been mapped to variable output. Each plugin may only be mapped once.'));
      }

    }

  }

}

/**
 * Render API callback: Validates UI group name.
 *
 * Ensures UI group name is legal.
 *
 * This function is assigned as an #element_validate callback in
 * morris_omniture_mappings_form().
 */
function morris_omniture_mappings_form_validate_grp($element, &$form_state, $form) {

  if (isset($form_state['values']['op']) && strcasecmp($form_state['values']['op'], 'Add') === 0) {

    $grp = $form_state['values']['vars']['new']['group'];

    if (!empty($grp) && (strcasecmp($grp, 'new') === 0 || strcasecmp($grp, 'undef') === 0)) {
      form_error($element, t('Variable group names undef and new are illegal. Please choose another group name.'));
    }

  }

}

/*
 * ----------------------------------------------------------------------
 * AJAX callbacks
 * ----------------------------------------------------------------------
 */

/**
 * AJAX callback settings form.
 * 
 * @param array $form
 *   The form build array.
 * 
 * @param array $form_state
 *   The form state array.
 * 
 * @return array
 *   The form partial.
 */
function morris_omniture_mappings_form_ajax_var_settings(&$form, &$form_state) {
  return $form['vars_wrapper'];
}

/**
 * AJAX callback plugin linked list.
 * 
 * @param array $form
 *   The form build array.
 * 
 * @param array $form_state
 *   The form state array.
 * 
 * @return array
 *   The form partial.
 */
function morris_omniture_mappings_form_ajax_plugin(&$form, &$form_state) {
  return $form['vars_wrapper']['vars']['new']['plugin_wrapper'];
}

/**
 * AJAX callback add new variable mapping.
 * 
 * @param array $form
 *   The form build array.
 * 
 * @param array $form_state
 *   The form state array.
 * 
 * @return array
 *   The form partial.
 */
function morris_omniture_mappings_form_ajax_var_add(&$form, &$form_state) {
  return $form['vars_wrapper'];
}

/**
 * AJAX callback remove/delete variable mapping.
 * 
 * @param array $form
 *   The form build array.
 * 
 * @param array $form_state
 *   The form state array.
 * 
 * @return array
 *   The form partial.
 */
function morris_omniture_mappings_form_ajax_var_remove(&$form, &$form_state) {
  return $form['vars_wrapper'];
}

/**
 * AJAX callback plugin settings.
 * 
 * @param array $form
 *   The form build array.
 * 
 * @param array $form_state
 *   The form state array.
 * 
 * @return array
 *   The form partial.
 */
function morris_omniture_mappings_form_ajax_plugin_settings(&$form, &$form_state) {
  return $form['vars_wrapper'];
}

/**
 * ----------------------------------------------------------------------
 * Submit handlers
 * ----------------------------------------------------------------------
 */

/**
 * Form submission handler for morris_omniture_mappings_form().
 * 
 * Removes a mapping.
 *
 * @see morris_omniture_mappings_form()
 */
function morris_omniture_mappings_form_remove_handler($form, &$form_state) {

  $vars = morris_omniture_get_props();
  $remove_at = (int) str_replace('Remove ', '', $form_state['values']['op']);

  $item = NULL;

  $index = 0;
  foreach ($vars as $var) {
    if ($index == $remove_at) {
      $item = $var;
      break;
    }
    $index++;
  }

  if ($item !== NULL) {

    try {
      $deleted = entity_get_controller('morris_omniture_var')->purgeById(array($item->getId()));

      // Rebuild the form.
      $form_state['rebuild'] = TRUE;

      // Prompt the user.
      drupal_set_message("Variable {$item->getId()} has been deleted");

    }
    catch (Exception $e) {

      // Prompt the user.
      drupal_set_message("Unable to remove {$item->getId()} variable", 'error');

    }

  }

}

/**
 * Form submission handler for morris_omniture_mappings_form().
 * 
 * Add new variable mapping.
 *
 * @see morris_omniture_mappings_form()
 */
function morris_omniture_mappings_form_add_handler($form, &$form_state) {

  $values = $form_state['values'];

  // Create the domain object.
  $var = new MorrisOmniture_Var();
  $var->setId($values['vars']['new']['id']);
  $var->setGrp($values['vars']['new']['group']);

  // Bind the plugin.
  $plugin = morris_omniture_var_plugin_load($values['vars']['new']['plugin']);
  $var->setPlugin($plugin);

  try {

    // Persist variable mapping.
    entity_get_controller('morris_omniture_var')->persist($var);

    // Rebuild the form.
    $form_state['rebuild'] = TRUE;

    // Prompt the user.
    drupal_set_message("variable {$var->getId()} created");

  }
  catch (Exception $e) {
    drupal_set_message("Unable to create {$var->getId()} variable.", 'error');

  }

}

/**
 * Form submission handler for morris_omniture_mappings_form().
 * 
 * Save variable mapping settings.
 *
 * @see morris_omniture_mappings_form()
 */
function morris_omniture_mappings_form_plugin_settings_handler($form, &$form_state) {

  $vars = morris_omniture_get_props();
  $settings_at = (int) str_replace('Save ', '', $form_state['values']['op']);

  $item = NULL;

  $index = 0;
  foreach ($vars as $var) {
    if ($index == $settings_at) {
      $item = $var;
      break;
    }
    $index++;
  }

  $group = $item->getGrp() ? $item->getGrp() : 'undef';

  $plugin = $item->getPlugin();

  $values = $form_state['values'];

  // Hand off to plugin.
  $plugin->settings_submit($values['vars'][$group]["{$settings_at}_settings"]);

  // Dip out of the settings.
  $form_state['rebuild'] = TRUE;

  drupal_set_message("The {$item->getId()} variable bas been updated.");

}

/**
 * Form submission handler for morris_omniture_mappings_form().
 * 
 * Cancel settings change.
 *
 * @see morris_omniture_mappings_form()
 */
function morris_omniture_mappings_form_plugin_settings_handler_cancel($form, &$form_state) {

  // Dip out of the settings.
  $form_state['rebuild'] = TRUE;

}

/**
 * ----------------------------------------------------------------------
 * Post render callbacks
 * ----------------------------------------------------------------------
 */

/**
 * Add a fake submit element.
 *
 * This is necessary so that the value of actual submit can contain
 * the identifier int without displaying it. To do this the actual button
 * is hidden using CSS. Than another button takes the place of it that
 * dynamically triggers the mousedown event on the actual hidden submit
 * button.
 * 
 * @param str $content
 *   The content of the form element.
 * 
 * @param array $element
 *   The form element.
 * 
 * @return str
 *   The modified form element output.
 */
function morris_omniture_mappings_form_proxy_btn($content, $element) {

  $proxy = array(
    '#type' => 'submit',
    '#attributes' => array(

      // Remove the int identifier.
      'value' => preg_replace('/^(.*?)\s([0-9]*?)$/', '$1', $element['#value']),

      // The CSS and JavaScript hook.
      'class' => array('btn-proxy'),
    ),
  );

  if (isset($element['#disabled']) && $element['#disabled']) {
    $proxy['#attributes']['disabled'] = $element['#disabled'];
  }

  $content = drupal_render($proxy) . $content;

  return $content;
}

/*
 * ----------------------------------------------------------------------
 * Theme
 * ----------------------------------------------------------------------
 */

/**
 * Custom theme function to create intended display using table theme. 
 * 
 * Some of this is tightly coupled to the structure of
 * the form build array. Therefore, caution must be taken when
 * changes to the forms structure are made.
 * 
 * @param array $variables
 *   The theme variables.
 * 
 * @return str
 *   The theme HTML output.
 */
function theme_morris_omniture_mappings_form_var_table($variables) {

  $rows = array();

  // Number of total columns in table.
  $allow_edit = morris_omniture_user_var_mapping_admin();
  $total_cols = morris_omniture_user_var_mapping_admin() ? 4 : 3;

  // Get all the groups.
  $groups = element_children($variables['elements']);

  // Cycle through each group.
  foreach ($groups as $key) {

    /*
     * i === 0 indicates top/add new row
     * special handling is necssary because it is not grouped.
     */
    if (strcmp($key, 'new') === 0) {
      $props = element_children($variables['elements']);
    }
    else {
      // Add single separator row per group.
      $rows[] = array(
        array(
          'data' => $variables['elements'][$key]['#data'],
          'colspan' => $total_cols,
          'style' => 'background-color: #BEBFB9; color: white; font-weight: bolder;',
        ),
      );

      // Get each defined variable in group.
      $props = element_children($variables['elements'][$key]);
    }

    // Cycle through each property.
    foreach ($props as $prop_key) {

      // Isolate the child item.
      if (strcmp($key, 'new') === 0) {
        $child = $variables['elements'][$prop_key];
      }
      else {
        $child = $variables['elements'][$key][$prop_key];
      }

      // End cell data structure.
      $cells = array();

      /*
       * Isolate settings so that they can all be displayed
       * in single cell that spans full width of table.
       */
      $settings = array();

      /*
       * Add each form item that makes up property as single
       * cell in row per property.
       */
      foreach ($child as $item) {
        if (is_array($item) && (isset($item['#type']) || isset($item['#markup']))) {
          // The settings.
          if (strpos($prop_key, '_settings') !== FALSE) {
            $settings[] = $item;
          }
          else {
            $cell = array('data' => $item);
            $cells[] = $cell;
          }
        }
      }

      /*
       * Settings need to be shown in a single table cell
       * that spans full width of table.
       */
      if (strpos($prop_key, '_settings') !== FALSE) {
        $rows[] = array(
          array(
            'data' => $settings,
            'colspan' => $total_cols,
          ),
        );
      }
      else {
        $rows[] = $cells;
      }

    }

  }

  // Deal with an empty placeholder.
  if (($allow_edit && count($rows) === 1) || (!$allow_edit) && empty($rows)) {
    $rows[] = array(
      array(
        'data' => 'No variable mappings available',
        'colspan' => $total_cols,
      ),
    );
  }

  $variables['elements']['#rows'] = $rows;
  return theme('table', $variables['elements']);

}
