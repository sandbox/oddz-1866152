<?php
/**
 * @file
 * The configuration form, handlers and helper functions.
 */

/**
 * Form constructor for general configuration form.
 *
 * @see morris_omniture_config_form_submit()
 * @ingroup forms
 */
function morris_omniture_config_form($form, &$form_state) {

  $config = morris_omniture_default_config();
  
  $form['output_enabled'] = array(
    '#type'=> 'checkbox',
    '#title'=> 'Tracking Enabled',
    '#description'=> 'Determines whether pages will tracked.<br />This can be used to temporarily turn tracking off until a site is configured properly.',
    '#default_value'=> (int) $config['output_enabled']
  );

  $form['account'] = array(
    '#type' => 'textfield',
    '#title' => 'Account',
    '#description' => 'Sitecatalyst account.',
    '#default_value' => $config['account'],
    '#required' => TRUE,
  );

  $form['server'] = array(
    '#type' => 'textfield',
    '#title' => 'Server',
    '#description' => 'Sitecatalyst server variable.',
    '#default_value' => $config['server'],
    '#required' => TRUE,
  );

  $form['channel'] = array(
    '#type' => 'textfield',
    '#title' => 'Channel',
    '#description' => 'Sitecatalyst channel variable.',
    '#default_value' => $config['channel'],
    '#required' => TRUE,
  );

  // Link tracking.
  $form['link_tracking'] = array(
    '#type' => 'fieldset',
    '#title' => 'Link Tracking',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['link_tracking']['link_internal_filters'] = array(
    '#type' => 'textfield',
    '#title' => 'Filters',
    '#description' => 'Sitecatalyst s_code linkInternalFilters setting.',
    '#default_value' => $config['link_internal_filters'],
    '#required' => FALSE,
  );

  $form['link_tracking']['link_track_vars'] = array(
    '#type' => 'textfield',
    '#title' => 'Track Vars',
    '#description' => 'Sitecatalyst s_code linkTrackVars setting.',
    '#default_value' => $config['link_track_vars'],
    '#required' => FALSE,
  );

  $form['link_tracking']['link_download_file_types'] = array(
    '#type' => 'textfield',
    '#title' => 'Download File Types',
    '#description' => 'Sitecatalyst s_code linkDownloadFileTypes setting.',
    '#default_value' => $config['link_download_file_types'],
    '#required' => FALSE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Update',
  );

  return $form;
}

/**
 * Form submission handler for morris_omniture_config_form().
 */
function morris_omniture_config_form_submit($form, &$form_state) {

  // Isolate the valid fields.
  $fields = morris_omniture_config_fields();

  // The actual values to save.
  $values = array();

  // Extract all the submitted values.
  foreach ($form_state['values'] as $key => $value) {
    if (in_array($key, $fields)) {
      $values[$key] = $value;
    }
  }

  // Save the config.
  foreach ($values as $key => $value) {
    variable_set(MORRIS_OMNITURE_VAR_PREFIX . $key, $value);
  }

  // Prompt the user.
  drupal_set_message('SiteCatalyst configuration updated!');

}
