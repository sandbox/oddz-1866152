<?php
/**
 * @file
 * Page callback to dump current variable mappings as array.
 */

/**
 * Dump all variable mapping data.
 *
 * @return str 
 *   The php mapping variable build array.
 */
function morris_omniture_var_export_php() {

  $vars = entity_get_controller('morris_omniture_var')->load();

  $php = '$vars = array();' . PHP_EOL;
  $grp = $var->getGrp();

  foreach ($vars as $var) {
    $php .= sprintf('$vars[] = array(' . PHP_EOL . "\t" . '"id"=>"%s",' . PHP_EOL . "\t" . '"plugin"=>"%s",' . PHP_EOL . "\t" . '"grp"=>%s,' . PHP_EOL . "\t" . '"weight"=>%u' . PHP_EOL . ');' . PHP_EOL, $var->getId(), $var->getPlugin()->get_name(), empty($grp) ? 'NULL' : '"' . $grp . '"', $var->getWeight());
  }

  echo $php;

}
