<?php
/**
 * @file
 * Morris group plugin.
 */

$plugin = array(
  'class' => 'morris_omniture_var_morris_group',
  'title' => 'Group',
  'group' => 'Morris',
  'overridable' => FALSE,
);

class morris_omniture_var_morris_group extends morris_omniture_var_base {

  const VAR_NAME = 'morris_omniture_var_morris_group';

  /**
   * The settings form.
   * 
   * @param array &$form_state
   *   The form_state.
   * 
   * @return array
   *   Additional fields to add to settings form.
   */
  public function settings(&$form_state) {

    $form = array();

    $form['group'] = array(
      '#title' => 'Group',
      '#type' => 'select',
      '#options' => _morris_omniture_group_options(),
      '#default_value' => variable_get(self::VAR_NAME, NULL),
    );

    return $form;

  }

  /**
   * Settings form submit handler.
   * 
   * @param array $values
   *   The submitted values.
   */
  public function settings_submit($values) {
    variable_set(self::VAR_NAME, $values['group']);
  }

  /**
   * When variable is unmapped delete global.
   */
  public function unmap() {
    variable_del(self::VAR_NAME);
  }

  /**
   * The plugin output value.
   * 
   * @return mix
   *   The value to send to Site Catalyst.
   */
  public function val() {

    $default = variable_get(self::VAR_NAME, '');

    return $default;

  }

}
