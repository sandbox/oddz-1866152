<?php
/**
 * @file
 * Integration with Yahoo APT module. The number
 * of ads served on the page.
 */

$plugin = array(
  'class' => 'morris_omniture_var_yahoo_apt_ads',
  'title' => 'Ad Num',
  'group' => 'Yahoo APT',
  'overridable' => FALSE,
);

class morris_omniture_var_yahoo_apt_ads extends morris_omniture_var_base {

  /**
   * The number of ads served on the page.
   * 
   * @return str
   *   The number of ads served on the page.
   */
  public function val() {

    if (module_exists('yahoo_apt')) {

      // Get all active ad blocks.
      $ad_blocks = yahoo_apt_discover_adspots();

      // Return the count.
      return (string) count($ad_blocks);

    }

  }

}
