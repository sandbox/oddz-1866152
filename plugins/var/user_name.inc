<?php
/**
 * @file
 * The current authenticated users name.
 */

$plugin = array(
  'class' => 'morris_omniture_var_morris_user_name',
  'title' => 'Name',
  'group' => 'User',
  'overridable' => FALSE,
);

class morris_omniture_var_morris_user_name extends morris_omniture_var_base {

  /**
   * The end output value.
   * 
   * @return str
   *   The current authenticated users name.
   */
  public function val() {

    global $user;

    return $user && $user->uid ? $user->name : NULL;

  }

}
