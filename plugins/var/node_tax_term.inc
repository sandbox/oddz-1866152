<?php 
/**
 * @file
 * Node page taxonomy term mapping. I'm not reallt certain if
 * this is the best idea. I think it would be better to create
 * a plugin for a specific node field whcih needs to be tracked.
 * Otherwise, all I can really do is either take the first term
 * or have a setting for which vocabulary to track. However, using
 * a vocabulary it could only be set-up for a single term. Probably
 * the first term is better. Not critical, we will see.
 */
