<?php
/**
 * @file
 * Special morris path variable mapping.
 */

$plugin = array(
  'class' => 'morris_omniture_var_morris_path',
  'title' => 'Path',
  'group' => 'Morris',
  'overridable' => FALSE,
);

class morris_omniture_var_morris_path extends morris_omniture_var_base {

  /**
   * Get end output value.
   * 
   * @return StdClass
   *   XSS safe inline JavaScript wrapper.
   */
  public function val() {

    // Same logic as CASAA omniture plugin.
    $str = "(function() {

            var lparr = location.pathname.split('/');
            var toppath = lparr[1];
            var nxtpath = (lparr.length > 2)?lparr[2]:'Frontpage';

            return (toppath == '')?'Frontpage':(toppath + ' - ' + ((nxtpath != '')?nxtpath:'Frontpage'));

        })();";

    return morris_omniture_js_snippet_wrapper($str);

  }

}
